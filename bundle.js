"use strict" ;

/**
 * The VEGAS JS process library.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
import data from './index'

import sayHello , { skipHello } from 'vegas-js-core/src/hello'

const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : '<@NAME@>'        } ,
    description : { enumerable : true , value : '<@DESCRIPTION@>' } ,
    version     : { enumerable : true , value : '<@VERSION@>'     } ,
    license     : { enumerable : true , value : "<@LICENSE@>"     } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>'    }
});

const bundle =
{
    metas ,
    sayHello,
    skipHello,
    ...data
};

export default bundle ;

try
{
    if ( window )
    {
        window.addEventListener( 'load' , function load()
        {
            window.removeEventListener( "load", load, false ) ;
            sayHello(metas.name,metas.version,metas.url) ;
        }, false );
    }
}
catch( ignored ) {}