"use strict" ;

import ArrayMap from 'vegas-js-data/src/maps/ArrayMap'

import BatchTaskNext from './BatchTaskNext'
import TaskGroup     from './TaskGroup'

/**
 * Batchs a set of actions and run it in the same time.
 * @summary Batchs a set of actions and run it in the same time.
 * @param {string} [mode=normal] - Specifies the <code>mode</code> of the group. This <code>mode</code> can be <code>"normal"</code> (default), <code>"transient"</code> or <code>"everlasting"</code>.
 * @param {array} [actions=null] An optional array who contains Action references to initialize the chain.
 * @name BatchTask
 * @class
 * @memberof system.process
 * @extends system.process.TaskGroup
 * @example
 * let do1 = new system.process.Do() ;
 * let do2 = new system.process.Do() ;
 *
 * do1.something = function()
 * {
 *     console.log( "do1 something" ) ;
 * }
 *
 * do2.something = function()
 * {
 *     console.log( "do2 something" ) ;
 * }
 *
 * let finish = function( action )
 * {
 *     trace( "finish: " + action ) ;
 * };
 *
 * let start = function( action )
 * {
 *     trace( "start: " + action ) ;
 * };
 *
 * let batch = new system.process.BatchTask() ;
 *
 * batch.add( do1 ) ;
 * batch.add( do2 ) ;
 *
 * batch.verbose = true ;
 *
 * trace( 'batch   : ' + batch.toString(true) ) ;
 * trace( 'running : ' + batch.running ) ;
 * trace( 'length  : ' + batch.length ) ;
 *
 * batch.finishIt.connect(finish) ;
 * batch.startIt.connect(start) ;
 *
 * batch.run() ;
 */
class BatchTask extends TaskGroup
{
    /**
     * Creates a new BatchTask instance
     * @param {string} [mode='normal'] The mode of the batch task.
     * @param {Array} [actions=null] The collection of action to run with the batch.
     * @constructor
     */
    constructor( mode = 'normal' , actions = null )
    {
        super( mode , actions ) ;

        Object.defineProperties( this ,
        {
            _current  : { value : null , writable : true },
            _currents : { value : new ArrayMap() , writable : true },
            _next     : { value : new BatchTaskNext(this) }
        }) ;
    }

    /**
     * Indicates the current Action reference when the batch is in progress.
     * @memberof system.process.BatchTask
     * @type {system.process.Action}
     * @instance
     * @readonly
     */
    get current()
    {
        return this._current
    }

    /**
     * Returns a shallow copy of this object.
     * @return a shallow copy of this object.
     * @name clone
     * @memberof system.process.BatchTask
     * @function
     * @instance
     */
    clone = () => new BatchTask( this._mode , ( this._actions.length > 0 ? this._actions : null ) ) ;

    /**
     * Resume the chain.
     * @name resume
     * @memberof system.process.BatchTask
     * @function
     * @instance
     */
    resume = () =>
    {
        if ( this._stopped )
        {
            this._running = true ;
            this._stopped = false ;
            this.notifyResumed() ;

            let l = this._actions instanceof Array ? this._actions.length : 0 ;
            while( --l > -1 )
            {
                const { action } = this._actions[l] || {} ;
                if ( action )
                {
                    if ( action.hasOwnProperty('resume') )
                    {
                        action.resume() ;
                    }
                    else
                    {
                        this.next( action ) ; // finalize the action to clean the batch
                    }
                }
            }
        }
        else
        {
            this.run() ;
        }
    }

    /**
     * Launchs the chain process.
     * @name run
     * @memberof system.process.BatchTask
     * @function
     * @instance
     */
    run = () =>
    {
        if ( !this._running )
        {
            this.notifyStarted() ;

            this._currents.clear() ;
            this._stopped = false ;
            this._current = null ;

            if ( this._actions.length > 0 )
            {
                let actions = [] ;

                this._actions.forEach( entry =>
                {
                    const { action } = entry || {} ;
                    if ( action )
                    {
                        actions.push( action ) ;
                        this._currents.set( action , entry ) ;
                    }
                });

                actions.forEach( action => { action.run() ; } );
            }
            else
            {
                this.notifyFinished() ;
            }
        }
    }

    /**
     * Stops the task group.
     * @name stop
     * @memberof system.process.BatchTask
     * @function
     * @instance
     */
    stop = () =>
    {
        if ( this._running )
        {
            let l = this._actions instanceof Array ? this._actions.length : 0 ;
            while( --l > -1 )
            {
                const { action } = this._actions[l] || {} ;
                if ( action.hasOwnProperty('stop') && ( action.stop instanceof Function ) )
                {
                    action.stop() ;
                }
            }
            this._running = false ;
            this._stopped = true ;
            this.notifyStopped() ;
        }
    }
}

export default BatchTask ;