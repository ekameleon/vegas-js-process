"use strict" ;

import Lockable from './Lockable'

/**
 * Indicates if the specific objet is {@link system.process.Lockable|Lockable} or contains the <code>lock()</code> / <code>unlock()</code> / <code>isLocked()</code> methods.
 * @name isLockable
 * @memberof system.process
 * @function
 * @instance
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Lockable|Lockable}.
 */
const isLockable = target =>
{
    if( target )
    {
        return ( target instanceof Lockable )
            ||
             ( Boolean( target['isLocked'] ) && ( target.isLocked instanceof Function )
            && Boolean( target['lock'] )     && ( target.lock     instanceof Function )
            && Boolean( target['unlock'] )   && ( target.unlock   instanceof Function )
             ) ;
    }
    return false ;
}

export default isLockable ;