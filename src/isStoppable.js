"use strict" ;

import Stoppable from './Stoppable'

/**
 * Indicates if the specific objet is {@link system.process.Stoppable|Stoppable} and contains a <code>stop()</code> method.
 * @name isStoppable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Stoppable|Stoppable}.
 */
const isStoppable = target => target ? ( ( target instanceof Stoppable ) || ( Boolean( target['stop'] ) && ( target.stop instanceof Function ) ) ) : false ;

export default isStoppable ;