"use strict" ;

import Priority from './Priority'

/**
 * Indicates if the specific objet implements the {@link system.process.Priority|Priority} interface or contains a <code>priority</code> attribute.
 * @name isPrioritizable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object implements the {@link system.process.Priority|Priority} interface.
 */
const isPrioritizable = target =>
{
    if( target )
    {
        /*jshint -W069 */
        return (target instanceof Priority) || ( Boolean(target['priority']) && !(target.priority instanceof Function) ) ;
        /*jshint +W069 */
    }
    return false ;
}

export default isPrioritizable ;