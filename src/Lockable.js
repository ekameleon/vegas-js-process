"use strict" ;

/**
 * This interface is implemented by all objects lockable.
 * @name Lockable
 * @memberof system.process
 * @interface
 */
class Lockable
{
    constructor()
    {
        Object.defineProperties( this ,
        {
            /**
             * @protected
             */
            __lock__ : { writable : true  , value : false }
        }) ;
    }

    /**
     * Returns <code>true</code> if the object is locked.
     * @return <code>true</code> if the object is locked.
     * @name isLocked
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    isLocked = () => this.__lock__ ;

    /**
     * Locks the object.
     * @name lock
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    lock = () => { this.__lock__ = true ; }

    /**
     * Unlocks the object.
     * @name unlock
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    unlock = () => { this.__lock__ = false ; }
}


export default Lockable ;