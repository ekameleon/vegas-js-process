"use strict" ;

import Startable from './Startable'

/**
 * Indicates if the specific objet is {@link system.process.Startable|Startable} and contains a <code>start()</code> method.
 * @name isStartable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Startable|Startable}.
 */
const isStartable = target => target ? ( (target instanceof Startable) || ( Boolean( target['start'] ) && ( target.start instanceof Function ) ) ) : false ;

export default isStartable ;