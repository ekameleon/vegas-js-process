"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be prioritizable (Capable of being prioritized).
 * @summary This interface should be implemented by any class whose instances are intended to be prioritizable (Capable of being prioritized).
 * @name Priority
 * @memberof system.process
 * @interface
 */
class Priority
{
    /**
     * Creates a new Priority instance.
     * @constructor
     */
    constructor()
    {
        Object.defineProperties( this ,
        {
            /**
             * Determinate the priority value.
             * @memberof system.process.Priority
             * @type {number}
             * @instance
             * @default 0
             */
            priority :
            {
                get : function()
                {
                    return this._priority ;
                } ,
                set : function( value )
                {
                    this._priority = (value > 0 || value < 0) ? value : 0 ;
                }
            },

            /**
             * @private
             */
            _priority : { writable : true , value : 0 }
        }) ;
    }
}

export default Priority ;