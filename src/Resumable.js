"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be resumed.
 * @name Resumable
 * @memberof system.process
 * @interface
 */
class Resumable
{
    /**
     * Resumes the command process.
     * @name resume
     * @memberof system.process.Resetable
     * @function
     * @instance
     */
    resume = () =>
    {
        throw new Error( this + ' resume not implemented' );
    }
}

export default Resumable;