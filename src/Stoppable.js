"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be stopped.
 * @name Stoppable
 * @memberof system.process
 * @interface
 */
class Stoppable
{
    /**
     * Stops the command process.
     * @name start
     * @memberof system.process.Startable
     * @function
     * @instance
     */
    stop = () =>
    {
        throw new Error( this + ' stop not implemented' ) ;
    }
}

export default Stoppable ;