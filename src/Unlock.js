"use strict" ;

import Action from './Action'

import isLockable from './Lockable'

/**
 * Invoked this action to unlock a specific {@link system.process.Lockable|Lockable} object.
 * @summary Invoked to unlock a specific {@link system.process.Lockable|Lockable} object.
 * @name Unlock
 * @class
 * @memberof system.process
 * @extends system.process.Action
 * @example
 * let chain  = new system.process.Chain() ;
 * let unlock = new system.process.Unlock( chain ) ;
 *
 * chain.lock() ;
 *
 * unlock.run() ;
 *
 * trace( chain.isLocked() ) ;
 */
class Unlock extends Action
{
    /**
     * Creates a new Unlock instance.
     * @param target
     * @constructor
     */
    constructor( target )
    {
        super() ;
        this.target = target ;
    }
        /**
     * Creates a copy of the object.
     * @return a shallow copy of this object.
     * @name clone
     * @memberof system.process.Unlock
     * @function
     * @instance
     */
    clone = () => new Unlock( this.target ) ;

    /**
     * Run the process.
     * @name run
     * @memberof system.process.Unlock
     * @function
     * @instance
     */
    run = () =>
    {
        this.notifyStarted() ;
        if( isLockable( this.target ) && this.target.isLocked() )
        {
            this.target.unlock() ;
        }
        this.notifyFinished() ;
    }
}


export default Unlock ;