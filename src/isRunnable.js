"use strict" ;

import Runnable from './Runnable'

/**
 * Indicates if the specific objet is {@link system.process.Runnable|Runnable} and contains a <code>run()</code> method.
 * @name isRunnable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Runnable|Runnable}.
 */
const isRunnable = target =>
{
    if( target )
    {
        return ( target instanceof Runnable ) || ( target.hasOwnProperty('run') && ( target.run instanceof Function ) ) ;
    }
    return false ;
}

export default isRunnable ;