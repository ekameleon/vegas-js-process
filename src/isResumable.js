"use strict" ;

import Resumable from './Resumable'

/**
 * Indicates if the specific objet is {@link system.process.Resumable|Resumable} and contains a <code>resume()</code> method.
 * @name isResumable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Resumable|Resumable}.
 */
const isResumable = target => target ? ( target instanceof Resumable ) || ( Boolean( target['resume'] ) && ( target.resume instanceof Function ) ) : false ;

export default isResumable ;