"use strict" ;

import Resetable from './Resetable'

/**
 * Indicates if the specific objet is {@link system.process.Resetable|Resetable} and contains a <code>reset()</code> method.
 * @name isResetable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.process.Resetable|Resetable}.
 */
const isResetable = target =>
{
    if( target )
    {
        if( target instanceof Resetable )
        {
            return true ;
        }
        /*jshint -W069 */
        return Boolean( target['reset'] ) && ( target.reset instanceof Function )  ;
        /*jshint +W069 */
    }
    return false ;
}

export default isResetable ;