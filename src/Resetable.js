"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be reseted.
 * @name Resetable
 * @memberof system.process
 * @interface
 */
class Resetable
{
    /**
     * Resets the command process.
     * @name reset
     * @memberof system.process.Resetable
     * @function
     * @instance
     */
    reset = () =>
    {
        throw new Error( this + ' reset not implemented' ) ;
    }
}

export default Resetable ;