"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be started.
 * @name Startable
 * @memberof system.process
 * @interface
 */
class Startable
{
    /**
     * Starts the command process.
     * @name start
     * @memberof system.process.Startable
     * @function
     * @instance
     */
    start = () =>
    {
        throw new Error( this + ' start not implemented' ) ;
    }
}

export default Startable ;