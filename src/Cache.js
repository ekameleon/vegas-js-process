"use strict" ;

import notEmpty from 'vegas-js-core/src/strings/notEmpty'

import Property  from 'vegas-js-data/src/Property'
import Attribute from 'vegas-js-data/src/Attribute'
import Method    from 'vegas-js-data/src/Method'

import Action from './Action'

/**
 * Enqueue a collection of members definitions (commands) to apply or invoke with the specified target object.
 * @summary Enqueue a collection of members definitions (commands) to apply or invoke with the specified target object.
 * @name Cache
 * @class
 * @memberof system.process
 * @extends system.process.Action

 * @example
 * let Cache = system.process.Cache ;
 *
 * let object = {} ;
 *
 * object.a = 1 ;
 * object.b = 2 ;
 * object.c = 3 ;
 * object.d = 4 ;
 *
 * Object.defineProperties( object ,
 * {
 *     method1 :
 *     {
 *         value : function( value )
 *         {
 *             this.c = value ;
 *         }
 *     },
 *     method2 :
 *     {
 *         value : function( value1 , value2 )
 *         {
 *             this.d = value1 + value2 ;
 *         }
 *     }
 * });
 *
 * trace( object ) ; // {a:1,b:2,c:3,d:4}
 *
 * let cache = new Cache() ;
 *
 * cache.addAttribute( "a" , 10 ) ;
 * cache.addAttribute( "b" , 20 ) ;
 *
 * cache.addMethod( "method1" , 30 ) ;
 * cache.addMethodWithArguments( "method2" , [ 40 , 50 ] ) ;
 *
 * cache.target = object ;
 *
 * cache.run() ; // flush the cache and initialize the target or invoked this methods.
 *
 * trace( object ) ; // {a:10,b:20,c:30,d:90}
 * @see system.data.Property
 * @see system.data.Attribute
 * @see system.data.Method
 */
class Cache extends Action
{
    /**
     * Creates a new Cache instance.
     * @param {Object} [target=null] - The object to map with this cache process.
     * @param {Array} [init=null] - The <code>Array</code> of <code>Property</code> to map in the target reference when the process is running.
     * @constructor
     */
    constructor( target = null , init = null )
    {
        super() ;

        Object.defineProperties( this ,
        {
            /**
             * The target reference.
             * @memberof system.process.Cache
             * @instance
             * @type {object}
             */
            target : { value : target , writable : true } ,

            /**
             * @private
             */
            _queue : { value : [] , writable : true }
        }) ;

        if ( init instanceof Array && init.length > 0 )
        {
            init.forEach( prop => this._queue.push( prop ) ) ;
        }
    }

    /**
     * Returns the number of properties.
     * @name length
     * @memberof system.process.Cache
     * @instance
     * @readonly
     */
    get length()
    {
        return this._queue.length ;
    }

    /**
     * Enqueues a specific Property definition.
     * @name add
     * @memberof system.process.Cache
     * @instance
     * @function
     * @param {system.data.Property} property - The property to register.
     * @return The current <code>Cache</code> reference.
     * @see system.data.Attribute
     * @see system.data.Method
     */
    add = property =>
    {
        if ( property instanceof Property )
        {
            this._queue.push( property ) ;
        }
        return this ;
    }

    /**
     * Enqueues an attribute name/value entry.
     * @name addAttribute
     * @memberof system.process.Cache
     * @instance
     * @function
     * @param {string} name - The name of the attribute to register.
     * @param {*} value - The value of the attribute to register.
     * @return The current <code>Cache</code> reference.
     */
    addAttribute = ( name , value ) =>
    {
        if ( notEmpty(name) )
        {
            this._queue.push( new Attribute( name , value ) ) ;
        }
        return this ;
    }

    /**
     * Enqueues a method definition.
     * @name addMethod
     * @memberof system.process.Cache
     * @instance
     * @function
     * @param {string} name - The name of the method to register.
     * @param {Array} args - The optional parameters to fill in the method.
     * @return {Cache} The current <code>Cache</code> reference.
     */
    addMethod = ( name , ...args ) =>
    {
        if ( notEmpty(name) )
        {
            this._queue.push( new Method( name , [ ...args ] ) ) ;
        }
        return this ;
    }

    /**
     * Enqueues a method definition.
     * @name addMethodWithArguments
     * @memberof system.process.Cache
     * @instance
     * @function
     * @param {string} name - The name of the method to register.
     * @param {Array} args - The optional parameters to fill in the method.
     * @return {Cache} The current <code>Cache</code> reference.
     */
    addMethodWithArguments = ( name , args ) =>
    {
        if ( notEmpty(name) )
        {
            this._queue.push( new Method( name , args ) ) ;
        }
        return this ;
    }

    /**
     * Removes all commands in memory.
     * @name clear
     * @memberof system.process.Cache
     * @function
     * @instance
     */
    clear = () =>
    {
       this._queue.length = 0 ;
    }

    /**
     * Returns a shallow copy of this object.
     * @name clone
     * @memberof system.process.Cache
     * @function
     * @instance
     * @return a shallow copy of this object.
     */
    clone = () => new Cache( this.target , this._queue ) ;

    /**
     * Indicates if the tracker cache is empty.
     * @name isEmpty
     * @memberof system.process.Cache
     * @function
     * @instance
     */
    isEmpty = () => this._queue.length === 0 ;

    /**
     * Run the process.
     * @name run
     * @memberof system.process.Cache
     * @function
     * @instance
     */
    run = () =>
    {
        this.notifyStarted() ;
        if ( this.target )
        {
            let len = this._queue.length ;
            if ( len > 0 )
            {
                for ( let i = 0 ; i<len ; i++ )
                {
                    const item = this._queue.shift() ;
                    if ( item instanceof Method )
                    {
                        this._invokeMethod( item ) ;
                    }
                    else if ( item instanceof Attribute )
                    {
                        this._invokeAttribute( item ) ;
                    }
                }
            }
        }
        this.notifyFinished() ;
    }

    /**
     * @param {Attribute} item The method definition.
     * @private
     */
    _invokeAttribute = item =>
    {
        if ( item instanceof Attribute )
        {
            const { name } = item ;
            if ( notEmpty(name) )
            {
                this.target[name] = item.value ;
            }
        }
    }

    /**
     * @param {Method} item The method definition.
     * @private
     */
    _invokeMethod = item =>
    {
        if ( item instanceof Method && this.target )
        {
            const { args , name } = item ;
            if ( notEmpty(name) )
            {
                const { [name]:method } = this.target ;
                if( method instanceof Function )
                {
                    method.apply( this.target , args ) ;
                }
            }
        }
    }
}

export default Cache ;