"use strict" ;

import Action from './Action.js'

/**
 * A simple command to do something. Very usefull to test something in a complex process.
 * @summary A simple command to do something.
 * @name Do
 * @class
 * @memberof system.process
 * @extends system.process.Action
 * @example
 * let action = new system.process.Do() ;
 *
 * action.something = function()
 * {
 *     trace( "do something" ) ;
 * }
 *
 * let finish = function( action )
 * {
 *     let message = "finish: " + action.toString() ;
 *     trace( message ) ;
 * };
 *
 * let start = function( action )
 * {
 *     let message = "start: " + action.toString() ;
 *     trace( message ) ;
 * };
 *
 * action.finishIt.connect(finish) ;
 * action.startIt.connect(start) ;
 *
 * action.run() ;
 */
class Do extends Action
{
    /**
     * Creates a new Do instance.
     */
    constructor()
    {
        super() ;
    }

    /**
     * Creates a copy of the object.
     * @return a shallow copy of this object.
     * @name clone
     * @memberof system.process.Do
     * @function
     * @instance
     */
    clone = () => new Do() ;

    /**
     * Do something in this method (override it).
     * @name something
     * @memberof system.process.Do
     * @function
     * @instance
     */
    something = () =>
    {
        throw new Error( this + ' something not implemented' ) ;
    }

    /**
     * Run the process.
     * @memberof system.process.Do
     * @function
     * @instance
     */
    run = () =>
    {
        this.notifyStarted();
        if ( this.something instanceof Function )
        {
            this.something();
        }
        this.notifyFinished();
    }

}

export default Do ;