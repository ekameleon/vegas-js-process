/*jshint laxbreak: true*/
"use strict" ;

import Runnable from './Runnable.js'

/**
 * Enables you to apply a common {@link system.process.Action|Action} to a group of {@link system.process.Action|Action} objects.
 * <p>All {@link system.process.Action|Action} objects are processed as a single unit.<p>
 * <p>This class use an internal typed Collection to register all {system.process.Runnable|Runnable} objects.</p>
 * @summary Enables you to apply a common {@link system.process.Action|Action} to a group of {@link system.process.Action|Action} objects.
 * @name Batch
 * @class
 * @memberof system.process
 * @augments system.process.Runnable
 * @implements system.process.Runnable
 * @param {array} [init=null] - The optional Array of Runnable objects to fill the batch.
 * @example
 * function Command( name )
 * {
 *     this.name = name ;
 * }
 *
 * Command.prototype = Object.create( system.process.Runnable.prototype ) ;
 * Command.constructor = Command ;
 *
 * Command.prototype.run = function()
 * {
 *     trace( this.name + " run") ;
 * }
 *
 * Command.prototype.toString = function()
 * {
 *     return '[Command ' + this.name + ']' ;
 * }
 *
 * let batch = new system.process.Batch() ;
 *
 * batch.add( new Command( "command1" ) ) ;
 * batch.add( new Command( "command2" ) ) ;
 *
 * console.info( batch.length ) ;
 *
 * batch.run() ;
 */
class Batch extends Runnable
{
    constructor( init = null )
    {
        super() ;
        Object.defineProperties( this ,
        {
            _entries :
            {
                value        : [] ,
                enumerable   : false ,
                writable     : true ,
                configurable : false
            },
        }) ;

        if ( init && init instanceof Array && init.length > 0 )
        {
            init.forEach( ( element ) =>
            {
                if ( element instanceof Runnable )
                {
                    this.add( element ) ;
                }
            }) ;
        }
    }

    /**
     * Retrieves the number of elements in this batch.
     * @return the number of elements in this batch.
     * @name length
     * @memberof system.process.Batch
     * @instance
     * @readonly
     */
    get length()
    {
        return this._entries.length ;
    }

    /**
     * Adds the specified Runnable object in batch.
     * @param {system.process.Runnable} command - The command to register in the batch.
     * @return <code>true</code> if the command is registered.
     * @name add
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    add = command =>
    {
        if ( command && command instanceof Runnable )
        {
            this._entries.push( command ) ;
            return true ;
        }
        return false ;
    }

    /**
     * Removes all of the elements from this batch.
     * @name clear
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    clear = () =>
    {
        this._entries.length = 0 ;
    }

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     * @name clone
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    clone = () =>
    {
        let batch = new Batch() ;
        let l = this._entries.length ;
        for( let i = 0 ; i < l ; i++ )
        {
            batch.add( this._entries[i] ) ;
        }
        return batch ;
    }

    /**
     * Returns <pre>true</pre> if this batch contains the specified element.
     * @param {system.process.Runnable} command - The command to search in the batch.
     * @return {boolean} <pre>true</pre> if this batch contains the specified element.
     * @name contains
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    contains = command =>
    {
        if ( command instanceof Runnable )
        {
            let l = this._entries.length ;
            while( --l > -1 )
            {
                if ( this._entries[l] === command )
                {
                    return true ;
                }
            }
        }
        return false ;
    }

    /**
     * Returns the command from this batch at the passed index.
     * @param {*} key - The key to find a specific command in the batch.
     * @return the command from this batch at the passed index.
     * @name get
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    get = key => this._entries[key];

    /**
     * Returns the position of the passed object in the batch.
     * @param {Runnable} command the Runnable object to search in the collection.
     * @param {number} [fromIndex=0] the index to begin the search in the collection.
     * @return {number} the index of the object or -1 if the object isn't find in the batch.
     * @name indexOf
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    indexOf = ( command , fromIndex /*uint*/ = 0 ) =>
    {
        if ( command instanceof Runnable )
        {
            if ( isNaN( fromIndex ) )
            {
                fromIndex = 0 ;
            }
            fromIndex = ( fromIndex > 0 ) ? Math.round(fromIndex) : 0 ;
            let l = this._entries.length ;
            let i = fromIndex ;
            for( i ; i < l ; i++ )
            {
                if ( this._entries[i] === command )
                {
                    return i ;
                }
            }
        }
        return -1 ;
    }

    /**
     * Returns <pre>true</pre> if this batch contains no elements.
     * @return {boolean} True if this batch is empty else {@code false}.
     * @name isEmpty
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    isEmpty = () => this._entries.length === 0 ;

    /**
     * Removes a single instance of the specified element from this collection, if it is present (optional operation).
     * @param {Runnable} command - The command to register in the batch.
     * @return <code>true</code> if the command is removed.
     * @name remove
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    remove = command =>
    {
        let index = this.indexOf( command ) ;
        if ( index > -1 )
        {
            this._entries.splice( index , 1 ) ;
            return true ;
        }
        return false ;
    }

    /**
     * Run the process.
     * @name run
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    run = () =>
    {
        let l = this._entries.length ;
        if ( l > 0 )
        {
            let i = -1 ;
            while ( ++i < l )
            {
                this._entries[i].run() ;
            }
        }
    }

    /**
     * Stops all commands in the batch.
     * @name stop
     * @memberof system.process.Batch
     * @function
     * @instance
     */
    stop = () =>
    {
        let l = this._entries.length ;
        if (l > 0)
        {
            this._entries.forEach( ( element ) =>
            {
                if ( element instanceof Runnable
                     &&( 'stop' in element )
                     && ( element.stop instanceof Function ) )
                {
                    element.stop() ;
                }
            }) ;
        }
    }

    /**
     * Returns an array containing all of the elements in this batch.
     * @name toArray
     * @memberof system.process.Batch
     * @function
     * @instance
     * @return an array containing all of the elements in this batch.
     */
    toArray = () => this._entries.slice() ;

    /**
     * Returns the source code string representation of the object.
     * @name toString
     * @memberof system.process.Batch
     * @function
     * @instance
     * @return the source code string representation of the object.
     */
    toString = () =>
    {
        let r = "[" + this.constructor.name ;
        let l = this._entries.length ;
        if( l > 0 )
        {
            r += '[' ;
            this._entries.forEach( ( element , index ) =>
            {
                r += element ;
                if (index < (l-1))
                {
                    r += "," ;
                }
            }) ;
            r += ']' ;
        }
        r += "]";
        return r ;
    }
}

export default Batch ;