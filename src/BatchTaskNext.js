"use strict" ;

import Receiver from 'vegas-js-signals/src/Receiver'

import TaskGroup from './TaskGroup'

/**
 * The internal BatchTaskNext Receiver.
 * @summary The internal class used in the <code>BatchTask</code> class.
 * @name BatchTaskNext
 * @memberof system.process
 * @class
 * @private
 * @implements system.signals.Receiver
 * @param {system.process.BatchTask} BatchTask - The <code>BatchTask</code> reference of this receiver.
 */
class BatchTaskNext extends Receiver
{
    /**
     * Creates a new BatchTaskNext instance.
     * @param batch
     */
    constructor( batch )
    {
        super() ;

        /**
         * The batch to register in this helper.
         * @memberof system.process.BatchTaskNext
         * @type {system.process.BatchTask}
         * @instance
         */
        this.batch = batch ;
    }

    /**
     * Receives the signal message.
     * @name receive
     * @memberof system.transitions.BatchTaskNext
     * @function
     * @instance
     * @param {system.process.Action} action - The <code>Action</code> reference received in this slot.
     */
    receive = action =>
    {
        let batch    = this.batch ;
        let mode     = batch.mode ;
        let actions  = batch._actions ;
        let currents = batch._currents ;

        if ( action && currents.has( action ) )
        {
            let entry = currents.get( action ) ;

            if ( mode !== TaskGroup.EVERLASTING )
            {
                if ( mode === TaskGroup.TRANSIENT || ( entry.auto && mode === TaskGroup.NORMAL ) )
                {
                    let e ;
                    let l = actions.length ;
                    while( --l > -1 )
                    {
                        e = actions[l] ;
                        if( e && e.action === action )
                        {
                            action.finishIt.disconnect( this ) ;
                            actions.splice( l , 1 ) ;
                            break ;
                        }
                    }
                }
            }

            currents.delete( action ) ;
        }

        if( batch._current !== null )
        {
            batch.notifyChanged() ;
        }

        batch._current = action ;

        batch.notifyProgress() ;

        if( currents.length === 0 )
        {
            batch._current = null ;
            batch.notifyFinished();
        }
    }

    toString = () => '[' + this.constructor.name + ']' ;
}

export default BatchTaskNext ;