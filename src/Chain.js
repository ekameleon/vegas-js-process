"use strict" ;

import ChainNext from './ChainNext'
import TaskGroup from './TaskGroup'

/**
 * A chain is a sequence with a finite or infinite number of actions. All actions registered in the chain can be executed one by one with different strategies (loop, auto remove, etc).
 * @summary A chain is a sequence with a finite or infinite number of actions.
 * @name Chain
 * @class
 * @memberof system.process
 * @extends system.process.TaskGroup
 * @example
 * let do1 = new system.process.Do() ;
 * let do2 = new system.process.Do() ;
 *
 * do1.something = function()
 * {
 *     trace( "do1 something" ) ;
 * }
 *
 * do2.something = function()
 * {
 *     trace( "do2 something" ) ;
 * }
 *
 * let finish = function( action )
 * {
 *     trace( "finish: " + action ) ;
 * };
 *
 * let progress = function( action )
 * {
 *     trace( "progress: " + action ) ;
 * };
 *
 * let start = function( action )
 * {
 *     trace( "start: " + action ) ;
 * };
 *
 * let chain = new system.process.Chain() ;
 *
 * chain.finishIt.connect(finish) ;
 * chain.progressIt.connect(progress) ;
 * chain.startIt.connect(start) ;
 *
 * chain.add( do1 , 0 ) ;
 * chain.add( do2 , 2 , true) ;
 *
 * chain.verbose = true ;
 *
 * trace('---------') ;
 *
 * trace( 'chain   : ' + chain.toString(true) ) ;
 * trace( 'running : ' + chain.running ) ;
 * trace( 'length  : ' + chain.length ) ;
 *
 * trace('---------') ;
 *
 * chain.run() ;
 *
 * trace('---------') ;
 *
 * chain.run() ;

 */
class Chain extends TaskGroup
{
    /**
     * Creates a new Chain instance.
     * @param {boolean} [looping=false] Specifies whether playback of the clip should continue, or loop (default false).
     * @param {number} [numLoop=0] Specifies the number of the times the presentation should loop during playback.
     * @param {string} [mode=normal] - Specifies the <code>mode</code> of the group. This <code>mode</code> can be <code>"normal"</code> (default), <code>"transient"</code> or <code>"everlasting"</code>.
     * @param {array} [actions=null] An optional array who contains Action references to initialize the chain.
     * @constructor
     */
    constructor( looping = false , numLoop = 0 , mode = 'normal' , actions = null )
    {
        super( mode , actions ) ;

        Object.defineProperties( this ,
        {
            /**
             * Indicates if the chain loop when is finished.
             * @memberof system.process.Chain
             * @type {boolean}
             * @instance
             * @default <code>false</code>
             */
            looping : { value : Boolean( looping ) , writable : true } ,

            /**
             * The number of loops.
             * @memberof system.process.Chain
             * @type {number}
             * @instance
             * @default 0
             */
            numLoop :
            {
                value    : ( numLoop > 0 ) ? Math.round( numLoop ) : 0 ,
                writable : true
            },

            _current     : { value : null , writable : true } ,
            _currentLoop : { value : 0    , writable : true } ,
            _position    : { value : 0    , writable : true } ,
            _next        : { value : new ChainNext(this)    }
        }) ;
    }

    /**
     * Indicates the current Action reference when the process is in progress.
     * @memberof system.process.Chain
     * @type {system.process.Action}
     * @instance
     * @readonly
     */
    get current()
    {
        return this._current ? this._current.action : null ;
    }

    /**
     * Indicates the current countdown loop value.
     * @memberof system.process.Chain
     * @type {number}
     * @instance
     * @readonly
     */
    get currentLoop()
    {
        return this._currentLoop ;
    }

    /**
     * Indicates the current numeric position of the chain when is running.
     * @memberof system.process.Chain
     * @type {number}
     * @instance
     * @readonly
     */
    get position()
    {
        return this._position ;
    }

    /**
     * Creates a copy of the object.
     * @return a shallow copy of this object.
     * @name clone
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    clone = () => new Chain( this.looping , this.numLoop , this._mode , ( this._actions.length > 0 ? this._actions : null ) ) ;

    /**
     * Retrieves the next action reference in the chain with the current position.
     * @name element
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    element = () => this.hasNext() ? ( this._actions[ this._position ] ).action : null ;

    /**
     * Retrieves the next action reference in the chain with the current position.
     * @name hasNext
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    hasNext = () => this._position < this._actions.length ;

    /**
     * Resume the chain.
     * @name resume
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    resume = () =>
    {
        if ( this._stopped )
        {
            this._running = true ;
            this._stopped = false ;

            this.notifyResumed() ;

            if ( this._current && this._current.action )
            {
                if ( "resume" in this._current.action )
                {
                    this._current.action.resume() ;
                }
            }
            else
            {
                this._next.receive() ;
            }
        }
        else
        {
            this.run() ;
        }
    }

    /**
     * Launchs the chain process.
     * @name run
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    run = () =>
    {
        if ( !this._running )
        {
            this.notifyStarted() ;

            this._current     = null  ;
            this._stopped     = false ;
            this._position    = 0 ;
            this._currentLoop = 0 ;

            this._next.receive() ;
        }
    }

    /**
     * Stops the task group.
     * @name stop
     * @memberof system.process.Chain
     * @function
     * @instance
     */
    stop = () =>
    {
        if ( this._running )
        {
            if( this.current )
            {
                const { action : { stop } = {} } = this.current ;
                if ( stop instanceof Function )
                {
                    stop() ;
                }
            }
            this._running = false ;
            this._stopped = true ;
            this.notifyStopped() ;
        }
    }
}

export default Chain ;