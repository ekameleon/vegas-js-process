"use strict" ;

import Action from './Action'

import isLockable from './isLockable'

/**
 * Invoked to lock a specific {@link system.process.Lockable|Lockable} object.
 * @summary Invoked to lock a specific {@link system.process.Lockable|Lockable} object.
 * @name Lock
 * @class
 * @memberof system.process
 * @extends system.process.Action
 * @augments system.process.Action
 * @example
 * let chain = new system.process.Chain() ;
 * let lock  = new system.process.Lock( chain ) ;
 *
 * lock.run() ;
 *
 * trace( chain.isLocked() ) ;
 */
class Lock extends Action
{
    /**
     * Creates a new Lock instance.
     * @param target The target object to lock.
     */
    constructor( target )
    {
        super() ;
        this.target = target ;
    }

    /**
     * Creates a copy of the object.
     * @return a shallow copy of this object.
     * @name clone
     * @memberof system.process.Lock
     * @function
     * @instance
     */
    clone = () => new Lock( this.target ) ;

    /**
     * Run the process.
     * @memberof system.process.Lock
     * @function
     * @instance
     */
    run = () =>
    {
        this.notifyStarted() ;
        if( isLockable( this.target ) && !this.target.isLocked() )
        {
            this.target.lock() ;
        }
        this.notifyFinished() ;
    }
}

export default Lock ;
