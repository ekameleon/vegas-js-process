"use strict" ;

import Signal from 'vegas-js-signals/src/Signal'

import Chain     from '../../src/Chain'
import Do        from '../../src/Do'
import TaskGroup from '../../src/TaskGroup'

import TaskPhase from '../../src/TaskPhase'

import isLockable from '../../src/isLockable'
import isRunnable from '../../src/isRunnable'


import chai  from 'chai' ;
const assert = chai.assert ;

describe( 'system.process.Chain' , () =>
{
    describe( 'new Chain()' , () =>
    {
        let chain ;

        beforeEach( () =>
        {
            chain = new Chain() ;
        })

        it('Chain is a constructor function', () =>
        {
            assert.isFunction( Chain );
        });

        it('new Chain() instanceOf TaskGroup', () =>
        {
            assert.instanceOf( chain , TaskGroup );
        });

        it('new Chain() isLockable', () =>
        {
            assert.isTrue( isLockable(chain) );
        });

        it('new Chain() isRunnable', () =>
        {
            assert.isTrue( isRunnable(chain) );
        });

        it('new Chain().finishIt instanceOf Signal', () =>
        {
            assert.instanceOf( chain.finishIt , Signal );
        });

        it('new Chain().phase === TaskPhase.INACTIVE', () =>
        {
            assert.equal( chain.phase , TaskPhase.INACTIVE );
        });

        it('new Chain().running === false', () =>
        {
            assert.isFalse( chain.running );
        });

        it('new Chain().startIt instanceOf Signal', () =>
        {
            assert.instanceOf( chain.startIt , Signal );
        });

        it('new Chain().isLocked() === false', () =>
        {
            assert.isFalse( chain.isLocked() );
        });

        it('new Chain().run()', () =>
        {
            assert.property( chain , "run" );
            assert.isFunction( chain.run );
        });

        it('new Chain().toString() === "[Chain]"', () =>
        {
            assert.equal( chain.toString() , "[Chain]" );
        });

        it('new Chain().clone()', () =>
        {
            let clone  = chain.clone() ;
            assert.isNotNull( clone );
            assert.instanceOf( clone , Chain );
            assert.notEqual( clone , chain );
        });

        it('new Chain().isLocked()', () =>
        {
            assert.isFalse( chain.isLocked() );
        });

        it('new Chain().lock()', () =>
        {
            chain.lock() ;
            assert.isTrue( chain.isLocked() );
        });

        it('new Chain().unlock()', () =>
        {
            chain.lock() ;
            chain.unlock() ;
            assert.isFalse( chain.isLocked() );
        });

        it('new Chain().notifyStarted()', () =>
        {
            chain.notifyStarted() ;
            assert.isTrue( chain.running );
            assert.equal( chain.phase , TaskPhase.RUNNING );
        });

        it('new Chain().notifyFinished()', () =>
        {
            chain.notifyFinished() ;
            assert.isFalse( chain.running );
            assert.equal( chain.phase , TaskPhase.INACTIVE );
        });

        it('new Chain().mode === TaskGroup.NORMAL', () =>
        {
            assert.equal( chain.mode , TaskGroup.NORMAL );
        });

        it('new Chain().mode === TaskGroup.EVERLASTING', () =>
        {
            chain.mode = TaskGroup.EVERLASTING ;
            assert.equal( chain.mode , TaskGroup.EVERLASTING );
        });

        it('new Chain().mode === TaskGroup.TRANSIENT', () =>
        {
            chain.mode = TaskGroup.TRANSIENT ;
            assert.equal( chain.mode , TaskGroup.TRANSIENT );
        });

        it('new Chain().mode = unknown ==> TaskGroup.NORMAL', () =>
        {
            chain.mode = 'unknown' ;
            assert.equal( chain.mode , TaskGroup.NORMAL );
        });
    });

    describe( 'new Chain() with actions' , () =>
    {
        let value = 0 ;
        let order = [] ;

        let chain ;
        let do1 ;
        let do2 ;
        let do3 ;

        before( () =>
        {
            chain = new Chain() ;
            do1   = new Do() ;
            do2   = new Do() ;
            do3   = new Do() ;
        })

        beforeEach( () =>
        {

            do1.something = () =>
            {
                value++ ;
                order.push( 1 ) ;
            }

            do2.something = () =>
            {
                value++ ;
                order.push( 2 ) ;
            }

            do3.something = () =>
            {
                value++ ;
                order.push( 3 ) ;
            }
        })

        afterEach( () =>
        {
            chain.length = 0 ;
            value = 0 ;
            order = [] ;
        })

        it('chain.add two actions, length === 2', () =>
        {
            chain.add( do1 ) ;
            chain.add( do2 ) ;

            assert.equal( chain.length , 2 ) ;
        });

        it('chain.add two actions, contains', () =>
        {
            chain.add( do1 ) ;
            chain.add( do2 ) ;

            chain.run() ;

            assert.isTrue( chain.contains(do1)  ) ;
            assert.isTrue( chain.contains(do2)  ) ;
            assert.isFalse( chain.contains(do3) ) ;
        });

        it('chain.add two actions, run', () =>
        {
            chain.add( do1 ) ;
            chain.add( do2 ) ;

            chain.run() ;

            assert.equal( value , 2 ) ;
            assert.equal( order.toString() , '1,2' ) ;
        });
    }) ;
});