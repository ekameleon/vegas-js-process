# VEGAS JS PROCESS OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.6] - 2021-07-02
- Fix the FrameTimer class with the ES6 notation

## [1.0.5] - 2021-05-15
- Full ES6 refactoring
- Add Unit tests
- Replace the constructor functions by class
- Fix interfaces
- Use new Mocha configuration
- Update the package.json dependencies versions : 
  - vegas-js-core        : "1.0.32"
  - vegas-js-data        : "1.0.11" 
  - vegas-js-signals     : "1.3.7"
  - vegas-js-system-core : "1.0.7"
- Update rollup and babel + mocha unit tests

## [1.0.4] - 2020-03-17
### Changed
- package.json : "vegas-js-core": "1.0.19",
- package.json : "vegas-js-data": "1.0.7",
- package.json : "vegas-js-signals": "1.3.6",
- package.json : "vegas-js-system-core": "1.0.6",

## [1.0.1] - 2018-11-03
### Changed
- Package.json dependencies version :
  - "vegas-js-core": "1.0.5",
  - "vegas-js-data": "1.0.3",
  - "vegas-js-signals": "1.3.2",
  - "vegas-js-system-core": "1.0.4"

## [1.0.0] - 2018-11-03
### Added
* First version

