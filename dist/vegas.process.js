/**
 * A library allow you to create and manage asynchronous operations in your applications - version: 1.0.6 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.vegas_process = factory());
}(this, (function () { 'use strict';

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);

      if (enumerableOnly) {
        symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      }

      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  class Receiver {
    constructor() {}
    receive() {}
    toString() {
      return '[' + this.constructor.name + ']';
    }
  }

  class Signaler {
    constructor() {}
    get length() {
      return 0;
    }
    connect(receiver, priority = 0, autoDisconnect = false) {}
    connected() {}
    disconnect(receiver = null) {}
    emit(...values) {}
    hasReceiver(receiver) {
      return false;
    }
    toString() {
      return '[Signaler]';
    }
  }

  class Signal extends Signaler {
    constructor() {
      super();
      Object.defineProperties(this, {
        proxy: {
          value: null,
          configurable: true,
          writable: true
        },
        receivers: {
          writable: true,
          value: []
        }
      });
    }
    get length() {
      return this.receivers.length;
    }
    connect(receiver, priority = 0, autoDisconnect = false) {
      if (receiver === null) {
        return false;
      }
      autoDisconnect = autoDisconnect === true;
      priority = priority > 0 ? priority - priority % 1 : 0;
      if (typeof receiver === "function" || receiver instanceof Function || receiver instanceof Receiver || "receive" in receiver) {
        if (this.hasReceiver(receiver)) {
          return false;
        }
        this.receivers.push(new SignalEntry(receiver, priority, autoDisconnect));
        let i;
        let j;
        let a = this.receivers;
        let swap = function (j, k) {
          let temp = a[j];
          a[j] = a[k];
          a[k] = temp;
          return true;
        };
        let swapped = false;
        let l = a.length;
        for (i = 1; i < l; i++) {
          for (j = 0; j < l - i; j++) {
            if (a[j + 1].priority > a[j].priority) {
              swapped = swap(j, j + 1);
            }
          }
          if (!swapped) {
            break;
          }
        }
        return true;
      }
      return false;
    }
    connected() {
      return this.receivers.length > 0;
    }
    disconnect(receiver = null) {
      if (receiver === null) {
        if (this.receivers.length > 0) {
          this.receivers = [];
          return true;
        } else {
          return false;
        }
      }
      if (this.receivers.length > 0) {
        let l = this.receivers.length;
        while (--l > -1) {
          if (this.receivers[l].receiver === receiver) {
            this.receivers.splice(l, 1);
            return true;
          }
        }
      }
      return false;
    }
    emit(...values) {
      let l = this.receivers.length;
      if (l === 0) {
        return;
      }
      let i;
      let r = [];
      let a = this.receivers.slice();
      let e;
      let slot;
      for (i = 0; i < l; i++) {
        e = a[i];
        if (e.auto) {
          r.push(e);
        }
      }
      if (r.length > 0) {
        l = r.length;
        while (--l > -1) {
          i = this.receivers.indexOf(r[l]);
          if (i > -1) {
            this.receivers.splice(i, 1);
          }
        }
      }
      l = a.length;
      for (i = 0; i < l; i++) {
        slot = a[i].receiver;
        if (slot instanceof Function || typeof slot === "function") {
          slot.apply(this.proxy || this, values);
        } else if (slot instanceof Receiver || "receive" in slot && slot.receive instanceof Function) {
          slot.receive.apply(this.proxy || slot, values);
        }
      }
    }
    hasReceiver(receiver) {
      if (receiver === null) {
        return false;
      }
      if (this.receivers.length > 0) {
        let l = this.receivers.length;
        while (--l > -1) {
          if (this.receivers[l].receiver === receiver) {
            return true;
          }
        }
      }
      return false;
    }
    toArray() {
      if (this.receivers.length > 0) {
        return this.receivers.map(item => item.receiver);
      }
      return [];
    }
    toString() {
      return '[Signal]';
    }
  }
  class SignalEntry {
    constructor(receiver, priority = 0, auto = false) {
      this.auto = auto;
      this.receiver = receiver;
      this.priority = priority;
    }
    toString() {
      return '[SignalEntry]';
    }
  }

  var Runnable = function Runnable() {
    var _this = this;
    _classCallCheck(this, Runnable);
    _defineProperty(this, "run", function () {
      throw new Error(_this + ' run not implemented');
    });
  }
  ;

  var TaskPhase = Object.defineProperties({}, {
    ERROR: {
      value: 'error',
      enumerable: true
    },
    DELAYED: {
      value: 'delayed',
      enumerable: true
    },
    FINISHED: {
      value: 'finished',
      enumerable: true
    },
    INACTIVE: {
      value: 'inactive',
      enumerable: true
    },
    RUNNING: {
      value: 'running',
      enumerable: true
    },
    STOPPED: {
      value: 'stopped',
      enumerable: true
    },
    TIMEOUT: {
      value: 'timeout',
      enumerable: true
    }
  });

  var Action = function (_Runnable) {
    _inherits(Action, _Runnable);
    var _super = _createSuper(Action);
    function Action() {
      var _this;
      _classCallCheck(this, Action);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Action();
      });
      _defineProperty(_assertThisInitialized(_this), "isLocked", function () {
        return _this.__lock__;
      });
      _defineProperty(_assertThisInitialized(_this), "lock", function () {
        _this.__lock__ = true;
      });
      _defineProperty(_assertThisInitialized(_this), "notifyFinished", function () {
        _this._running = false;
        _this._phase = TaskPhase.FINISHED;
        _this.finishIt.emit(_assertThisInitialized(_this));
        _this._phase = TaskPhase.INACTIVE;
      });
      _defineProperty(_assertThisInitialized(_this), "notifyStarted", function () {
        _this._running = true;
        _this._phase = TaskPhase.RUNNING;
        _this.startIt.emit(_assertThisInitialized(_this));
      });
      _defineProperty(_assertThisInitialized(_this), "unlock", function () {
        _this.__lock__ = false;
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        return '[' + _this.constructor.name + ']';
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        finishIt: {
          value: new Signal()
        },
        startIt: {
          value: new Signal()
        },
        __lock__: {
          writable: true,
          value: false
        },
        _phase: {
          writable: true,
          value: TaskPhase.INACTIVE
        },
        _running: {
          writable: true,
          value: false
        }
      });
      return _this;
    }
    _createClass(Action, [{
      key: "phase",
      get: function get() {
        return this._phase;
      }
    }, {
      key: "running",
      get: function get() {
        return this._running;
      }
    }]);
    return Action;
  }(Runnable);

  var ActionEntry = function ActionEntry(action) {
    var _this = this;
    var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var auto = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    _classCallCheck(this, ActionEntry);
    _defineProperty(this, "toString", function () {
      return "[" + _this.constructor.name + " action:" + _this.action + " priority:" + _this.priority + " auto:" + _this.auto + "]";
    });
    Object.defineProperties(this, {
      action: {
        writable: true,
        value: action
      },
      auto: {
        writable: true,
        value: auto === true
      },
      priority: {
        writable: true,
        value: priority > 0 ? Math.ceil(priority) : 0
      }
    });
  }
  ;

  var Apply = function (_Action) {
    _inherits(Apply, _Action);
    var _super = _createSuper(Apply);
    function Apply() {
      var _this;
      var func = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var scope = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      _classCallCheck(this, Apply);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Apply(_this.func, _this.args, _this.scope);
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (_this.func instanceof Function) {
          if (_this.args && _this.args.length > 0) {
            _this.func.apply(_this.scope, _this.args);
          } else {
            _this.func.apply(_this.scope);
          }
        } else {
          throw new TypeError(_assertThisInitialized(_this) + ' run failed, the \'func\' property must be a Function.');
        }
        _this.notifyFinished();
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        args: {
          value: args instanceof Array ? args : null,
          writable: true
        },
        func: {
          value: func instanceof Function ? func : null,
          writable: true
        },
        scope: {
          value: scope,
          writable: true
        }
      });
      return _this;
    }
    return Apply;
  }(Action);

  var Batch = function (_Runnable) {
    _inherits(Batch, _Runnable);
    var _super = _createSuper(Batch);
    function Batch() {
      var _this;
      var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      _classCallCheck(this, Batch);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "add", function (command) {
        if (command && command instanceof Runnable) {
          _this._entries.push(command);
          return true;
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "clear", function () {
        _this._entries.length = 0;
      });
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        var batch = new Batch();
        var l = _this._entries.length;
        for (var i = 0; i < l; i++) {
          batch.add(_this._entries[i]);
        }
        return batch;
      });
      _defineProperty(_assertThisInitialized(_this), "contains", function (command) {
        if (command instanceof Runnable) {
          var l = _this._entries.length;
          while (--l > -1) {
            if (_this._entries[l] === command) {
              return true;
            }
          }
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "get", function (key) {
        return _this._entries[key];
      });
      _defineProperty(_assertThisInitialized(_this), "indexOf", function (command) {
        var fromIndex
        = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        if (command instanceof Runnable) {
          if (isNaN(fromIndex)) {
            fromIndex = 0;
          }
          fromIndex = fromIndex > 0 ? Math.round(fromIndex) : 0;
          var l = _this._entries.length;
          var i = fromIndex;
          for (i; i < l; i++) {
            if (_this._entries[i] === command) {
              return i;
            }
          }
        }
        return -1;
      });
      _defineProperty(_assertThisInitialized(_this), "isEmpty", function () {
        return _this._entries.length === 0;
      });
      _defineProperty(_assertThisInitialized(_this), "remove", function (command) {
        var index = _this.indexOf(command);
        if (index > -1) {
          _this._entries.splice(index, 1);
          return true;
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        var l = _this._entries.length;
        if (l > 0) {
          var i = -1;
          while (++i < l) {
            _this._entries[i].run();
          }
        }
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {
        var l = _this._entries.length;
        if (l > 0) {
          _this._entries.forEach(function (element) {
            if (element instanceof Runnable && 'stop' in element && element.stop instanceof Function) {
              element.stop();
            }
          });
        }
      });
      _defineProperty(_assertThisInitialized(_this), "toArray", function () {
        return _this._entries.slice();
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        var r = "[" + _this.constructor.name;
        var l = _this._entries.length;
        if (l > 0) {
          r += '[';
          _this._entries.forEach(function (element, index) {
            r += element;
            if (index < l - 1) {
              r += ",";
            }
          });
          r += ']';
        }
        r += "]";
        return r;
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        _entries: {
          value: [],
          enumerable: false,
          writable: true,
          configurable: false
        }
      });
      if (init && init instanceof Array && init.length > 0) {
        init.forEach(function (element) {
          if (element instanceof Runnable) {
            _this.add(element);
          }
        });
      }
      return _this;
    }
    _createClass(Batch, [{
      key: "length",
      get: function get() {
        return this._entries.length;
      }
    }]);
    return Batch;
  }(Runnable);

  class KeyValuePair {
    get length() {
      return 0;
    }
    clear = () => {};
    clone = () => new KeyValuePair();
    copyFrom = map => {};
    delete = key => {};
    forEach = (callback, thisArg = null) => {};
    get = key => null;
    has = key => false;
    hasValue = value => false;
    isEmpty = () => true;
    iterator = () => null;
    keyIterator = () => null;
    keys = () => null;
    set = (key, value) => {};
    toString = () => '[' + this.constructor.name + ']';
    values = () => null;
  }

  function MapFormatter() {}
  MapFormatter.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: MapFormatter
    },
    format: {
      value: function (value) {
        if (value instanceof KeyValuePair) {
          let r = "{";
          let keys = value.keys();
          let len = keys.length;
          if (len > 0) {
            let values = value.values();
            for (let i = 0; i < len; i++) {
              r += keys[i] + ':' + values[i];
              if (i < len - 1) {
                r += ",";
              }
            }
          }
          r += "}";
          return r;
        } else {
          return "{}";
        }
      }
    }
  });

  const formatter = new MapFormatter();

  function MapEntry(key, value) {
    Object.defineProperties(this, {
      key: {
        value: key,
        writable: true
      },
      value: {
        value: value,
        writable: true
      }
    });
  }
  MapEntry.prototype = Object.create(Object.prototype, {
    constructor: {
      value: MapEntry
    },
    clone: {
      value: function () {
        return new MapEntry(this.key, this.value);
      }
    },
    toString: {
      value: function () {
        return "[MapEntry key:" + this.key + " value:" + this.value + "]";
      }
    }
  });

  class Iterator {
    delete = () => {};
    hasNext = () => false;
    key = () => null;
    next = () => null;
    reset = () => {};
    seek = position => {};
    toString = () => '[' + this.constructor.name + ']';
  }

  function ArrayIterator(array) {
    if (!(array instanceof Array)) {
      throw new ReferenceError(this + " constructor failed, the passed-in Array argument not must be 'null'.");
    }
    Object.defineProperties(this, {
      _a: {
        value: array,
        writable: true
      },
      _k: {
        value: -1,
        writable: true
      }
    });
  }
  ArrayIterator.prototype = Object.create(Iterator.prototype, {
    constructor: {
      value: ArrayIterator
    },
    delete: {
      value: function () {
        return this._a.splice(this._k--, 1)[0];
      }
    },
    hasNext: {
      value: function () {
        return this._k < this._a.length - 1;
      }
    },
    key: {
      value: function () {
        return this._k;
      }
    },
    next: {
      value: function () {
        return this._a[++this._k];
      }
    },
    reset: {
      value: function () {
        this._k = -1;
      }
    },
    seek: {
      value: function (position) {
        position = Math.max(Math.min(position - 1, this._a.length), -1);
        this._k = isNaN(position) ? -1 : position;
      }
    }
  });

  function MapIterator(map) {
    if (map && map instanceof KeyValuePair) {
      Object.defineProperties(this, {
        _m: {
          value: map,
          writable: true
        },
        _i: {
          value: new ArrayIterator(map.keys()),
          writable: true
        },
        _k: {
          value: null,
          writable: true
        }
      });
    } else {
      throw new ReferenceError(this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.");
    }
  }
  MapIterator.prototype = Object.create(Iterator.prototype, {
    constructor: {
      writable: true,
      value: MapIterator
    },
    delete: {
      value: function () {
        this._i.delete();
        return this._m.delete(this._k);
      }
    },
    hasNext: {
      value: function () {
        return this._i.hasNext();
      }
    },
    key: {
      value: function () {
        return this._k;
      }
    },
    next: {
      value: function () {
        this._k = this._i.next();
        return this._m.get(this._k);
      }
    },
    reset: {
      value: function () {
        this._i.reset();
      }
    },
    seek: {
      value: function (position) {
        throw new Error("This Iterator does not support the seek() method.");
      }
    }
  });

  function ArrayMap(keys = null, values = null) {
    Object.defineProperties(this, {
      _keys: {
        value: [],
        writable: true
      },
      _values: {
        value: [],
        writable: true
      }
    });
    if (keys === null || values === null) {
      this._keys = [];
      this._values = [];
    } else {
      let b = keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length;
      this._keys = b ? [].concat(keys) : [];
      this._values = b ? [].concat(values) : [];
    }
  }
  ArrayMap.prototype = Object.create(KeyValuePair.prototype, {
    constructor: {
      writable: true,
      value: ArrayMap
    },
    length: {
      get: function () {
        return this._keys.length;
      }
    },
    clear: {
      value: function () {
        this._keys = [];
        this._values = [];
      }
    },
    clone: {
      value: function () {
        return new ArrayMap(this._keys, this._values);
      }
    },
    copyFrom: {
      value: function (map) {
        if (!map || !(map instanceof KeyValuePair)) {
          return;
        }
        let keys = map.keys();
        let values = map.values();
        let l = keys.length;
        for (let i = 0; i < l; i = i - -1) {
          this.set(keys[i], values[i]);
        }
      }
    },
    delete: {
      value: function (key) {
        let v = null;
        let i = this.indexOfKey(key);
        if (i > -1) {
          v = this._values[i];
          this._keys.splice(i, 1);
          this._values.splice(i, 1);
        }
        return v;
      }
    },
    forEach: {
      value: function (callback, thisArg = null) {
        if (typeof callback !== "function") {
          throw new TypeError(callback + ' is not a function');
        }
        let l = this._keys.length;
        for (let i = 0; i < l; i++) {
          callback.call(thisArg, this._values[i], this._keys[i], this);
        }
      }
    },
    get: {
      value: function (key) {
        return this._values[this.indexOfKey(key)];
      }
    },
    getKeyAt: {
      value: function (index) {
        return this._keys[index];
      }
    },
    getValueAt: {
      value: function (index
      ) {
        return this._values[index];
      }
    },
    has: {
      value: function (key) {
        return this.indexOfKey(key) > -1;
      }
    },
    hasValue: {
      value: function (value) {
        return this.indexOfValue(value) > -1;
      }
    },
    indexOfKey: {
      value: function (key) {
        let l = this._keys.length;
        while (--l > -1) {
          if (this._keys[l] === key) {
            return l;
          }
        }
        return -1;
      }
    },
    indexOfValue: {
      value: function (value) {
        let l = this._values.length;
        while (--l > -1) {
          if (this._values[l] === value) {
            return l;
          }
        }
        return -1;
      }
    },
    isEmpty: {
      value: function () {
        return this._keys.length === 0;
      }
    },
    iterator: {
      value: function () {
        return new MapIterator(this);
      }
    },
    keyIterator: {
      value: function ()
      {
        return new ArrayIterator(this._keys);
      }
    },
    keys: {
      value: function () {
        return this._keys.concat();
      }
    },
    set: {
      value: function (key, value) {
        let r = null;
        let i = this.indexOfKey(key);
        if (i < 0) {
          this._keys.push(key);
          this._values.push(value);
        } else {
          r = this._values[i];
          this._values[i] = value;
        }
        return r;
      }
    },
    setKeyAt: {
      value: function (index, key) {
        if (index >= this._keys.length) {
          throw new RangeError("ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.");
        }
        if (this.containsKey(key)) {
          return null;
        }
        let k = this._keys[index];
        if (k === undefined) {
          return null;
        }
        let v = this._values[index];
        this._keys[index] = key;
        return new MapEntry(k, v);
      }
    },
    setValueAt: {
      value: function (index, value) {
        if (index >= this._keys.length) {
          throw new RangeError("ArrayMap.setValueAt(" + index + ") failed with an index out of the range.");
        }
        let v = this._values[index];
        if (v === undefined) {
          return null;
        }
        let k = this._keys[index];
        this._values[index] = value;
        return new MapEntry(k, v);
      }
    },
    toString: {
      value: function () {
        return formatter.format(this);
      }
    },
    values: {
      value: function () {
        return this._values.concat();
      }
    }
  });

  const isNotNull = value => value !== null;

  function swap(ar, from = 0, to = 0, clone = false) {
    if (ar instanceof Array) {
      if (clone) {
        ar = [...ar];
      }
      let value = ar[from];
      ar[from] = ar[to];
      ar[to] = value;
      return ar;
    }
    return null;
  }

  var Task = function (_Action) {
    _inherits(Task, _Action);
    var _super = _createSuper(Task);
    function Task() {
      var _this;
      _classCallCheck(this, Task);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Task();
      });
      _defineProperty(_assertThisInitialized(_this), "notifyChanged", function () {
        if (!_this.__lock__) {
          _this.changeIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyCleared", function () {
        if (!_this.__lock__) {
          _this.clearIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyError", function () {
        var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        _this._running = false;
        _this._phase = TaskPhase.ERROR;
        if (!_this.__lock__) {
          _this.errorIt.emit(_assertThisInitialized(_this), message);
        }
        if (_this.throwError) {
          throw new Error(message);
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyInfo", function (info) {
        if (!_this.__lock__) {
          _this.infoIt.emit(_assertThisInitialized(_this), info);
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyLooped", function () {
        _this._running = true;
        _this._phase = TaskPhase.RUNNING;
        if (!_this.__lock__) {
          _this.loopIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyPaused", function () {
        _this._running = false;
        _this._phase = TaskPhase.STOPPED;
        if (!_this.__lock__) {
          _this.pauseIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyProgress", function () {
        _this._running = true;
        _this._phase = TaskPhase.RUNNING;
        if (!_this.__lock__) {
          _this.progressIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyResumed", function () {
        _this._running = true;
        _this._phase = TaskPhase.RUNNING;
        if (!_this.__lock__) {
          _this.resumeIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyStopped", function () {
        _this._running = false;
        _this._phase = TaskPhase.STOPPED;
        if (!_this.__lock__) {
          _this.stopIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "notifyTimeout", function () {
        _this._running = false;
        _this._phase = TaskPhase.TIMEOUT;
        if (!_this.__lock__) {
          _this.timeoutIt.emit(_assertThisInitialized(_this));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "resume", function () {});
      _defineProperty(_assertThisInitialized(_this), "reset", function () {});
      _defineProperty(_assertThisInitialized(_this), "start", function () {
        _this.run();
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {});
      Object.defineProperties(_assertThisInitialized(_this), {
        changeIt: {
          value: new Signal()
        },
        clearIt: {
          value: new Signal()
        },
        errorIt: {
          value: new Signal()
        },
        infoIt: {
          value: new Signal()
        },
        looping: {
          value: false,
          writable: true
        },
        loopIt: {
          value: new Signal()
        },
        pauseIt: {
          value: new Signal()
        },
        progressIt: {
          value: new Signal()
        },
        resumeIt: {
          value: new Signal()
        },
        stopIt: {
          value: new Signal()
        },
        throwError: {
          value: false,
          writable: true
        },
        timeoutIt: {
          value: new Signal()
        }
      });
      return _this;
    }
    return Task;
  }(Action);

  var TaskGroup = function (_Task) {
    _inherits(TaskGroup, _Task);
    var _super = _createSuper(TaskGroup);
    function TaskGroup() {
      var _this;
      var mode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'normal';
      var actions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      _classCallCheck(this, TaskGroup);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "add", function (action) {
        var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var autoRemove = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        if (_this._running) {
          throw new Error(_assertThisInitialized(_this) + " add failed, the process is in progress.");
        }
        if (action instanceof Action) {
          autoRemove = autoRemove === true;
          priority = priority > 0 ? Math.round(priority) : 0;
          if (_this._next) {
            action.finishIt.connect(_this._next);
          }
          _this._actions.push(new ActionEntry(action, priority, autoRemove));
          var a = _this._actions;
          var swapped;
          var l = a.length;
          for (var i = 1; i < l; i++) {
            for (var j = 0; j < l - i; j++) {
              if (a[j + 1].priority > a[j].priority) {
                swapped = swap(_this._actions, j, j + 1);
              }
            }
            if (!swapped) {
              break;
            }
          }
          return true;
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new TaskGroup(_this._mode, _this._actions.length > 0 ? _this._actions : null);
      });
      _defineProperty(_assertThisInitialized(_this), "contains", function (action) {
        if (action && action instanceof Action) {
          if (_this._actions.length > 0) {
            var e
            ;
            var l = _this._actions.length;
            while (--l > -1) {
              e = _this._actions[l];
              if (e && e.action === action) {
                return true;
              }
            }
          }
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "dispose", function () {
        if (_this._actions.length > 0) {
          _this._actions.forEach(function (entry) {
            if (entry instanceof ActionEntry) {
              entry.action.finishIt.disconnect(_this._next);
            }
          });
        }
      });
      _defineProperty(_assertThisInitialized(_this), "get", function (index) {
        if (_this._actions.length > 0 && index < _this._actions.length) {
          var _ref = _this._actions[index] || {},
              action = _ref.action;
          return action;
        }
        return null;
      });
      _defineProperty(_assertThisInitialized(_this), "isEmpty", function () {
        return _this._actions.length === 0;
      });
      _defineProperty(_assertThisInitialized(_this), "next", function () {
        throw new Error(_assertThisInitialized(_this) + ' next not implemented');
      });
      _defineProperty(_assertThisInitialized(_this), "remove", function (action) {
        if (_this._running) {
          throw new Error(_assertThisInitialized(_this) + " remove failed, the process is in progress.");
        }
        _this.stop();
        if (_this._actions.length > 0) {
          if (action && action instanceof Action) {
            var e;
            var l = _this._actions.length;
            _this._actions.forEach(function (element) {
              if (element && element instanceof ActionEntry && element.action === action) {
                if (_this._next) {
                  e.action.finishIt.disconnect(_this._next);
                }
                _this._actions.splice(l, 1);
                return true;
              }
            });
          } else {
            _this.dispose();
            _this._actions.length = 0;
            _this.notifyCleared();
            return true;
          }
        }
        return false;
      });
      _defineProperty(_assertThisInitialized(_this), "toArray", function () {
        return _this._actions.length > 0 ? _this._actions.map(function (element) {
          return element && element.hasOwnProperty('action') ? element.action : null;
        }).filter(isNotNull) : [];
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        var s = "[" + _this.constructor.name;
        if (_this.verbose === true) {
          var len = _this._actions.length;
          if (len > 0) {
            var r = [];
            for (var i = 0; i < len; i++) {
              var action = _this._actions[i].action;
              r.push(action);
            }
            s += '[' + r.toString() + ']';
          }
        }
        s += "]";
        return s;
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        verbose: {
          value: false,
          writable: true
        },
        _actions: {
          value: [],
          writable: true
        },
        _next: {
          value: null,
          writable: true,
          configurable: true
        },
        _stopped: {
          value: false,
          writable: true
        },
        _mode: {
          value: TaskGroup.NORMAL,
          writable: true
        }
      });
      if (typeof mode === "string" || mode instanceof String) {
        _this.mode = mode;
      }
      if (actions && actions instanceof Array && actions.length > 0) {
        actions.forEach(function (action) {
          if (action instanceof Action) {
            _this.add(action);
          }
        });
      }
      return _this;
    }
    _createClass(TaskGroup, [{
      key: "length",
      get: function get() {
        return this._actions instanceof Array ? this._actions.length : 0;
      },
      set: function set(value) {
        if (this._running) {
          throw new Error(this + " length property can't be changed, the batch process is in progress.");
        }
        this.dispose();
        var old
        = this._actions.length;
        this._actions.length = value;
        var l = this._actions.length;
        if (l > 0) {
          while (--l > -1) {
            var entry = this._actions[l];
            if (entry && entry.action && this._next) {
              entry.action.finishIt.connect(this._next);
            }
          }
        } else if (old > 0) {
          this.notifyCleared();
        }
      }
    }, {
      key: "mode",
      get: function get() {
        return this._mode;
      },
      set: function set(value) {
        this._mode = value === TaskGroup.TRANSIENT || value === TaskGroup.EVERLASTING ? value : TaskGroup.NORMAL;
      }
    }, {
      key: "stopped",
      get: function get() {
        return this._stopped;
      }
    }]);
    return TaskGroup;
  }(Task);
  Object.defineProperties(TaskGroup, {
    EVERLASTING: {
      value: 'everlasting',
      enumerable: true
    },
    NORMAL: {
      value: 'normal',
      enumerable: true
    },
    TRANSIENT: {
      value: 'transient',
      enumerable: true
    }
  });

  var BatchTaskNext = function (_Receiver) {
    _inherits(BatchTaskNext, _Receiver);
    var _super = _createSuper(BatchTaskNext);
    function BatchTaskNext(_batch) {
      var _this;
      _classCallCheck(this, BatchTaskNext);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "receive", function (action) {
        var batch = _this.batch;
        var mode = batch.mode;
        var actions = batch._actions;
        var currents = batch._currents;
        if (action && currents.has(action)) {
          var entry = currents.get(action);
          if (mode !== TaskGroup.EVERLASTING) {
            if (mode === TaskGroup.TRANSIENT || entry.auto && mode === TaskGroup.NORMAL) {
              var e;
              var l = actions.length;
              while (--l > -1) {
                e = actions[l];
                if (e && e.action === action) {
                  action.finishIt.disconnect(_assertThisInitialized(_this));
                  actions.splice(l, 1);
                  break;
                }
              }
            }
          }
          currents["delete"](action);
        }
        if (batch._current !== null) {
          batch.notifyChanged();
        }
        batch._current = action;
        batch.notifyProgress();
        if (currents.length === 0) {
          batch._current = null;
          batch.notifyFinished();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        return '[' + _this.constructor.name + ']';
      });
      _this.batch = _batch;
      return _this;
    }
    return BatchTaskNext;
  }(Receiver);

  var BatchTask = function (_TaskGroup) {
    _inherits(BatchTask, _TaskGroup);
    var _super = _createSuper(BatchTask);
    function BatchTask() {
      var _this;
      var mode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'normal';
      var _actions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      _classCallCheck(this, BatchTask);
      _this = _super.call(this, mode, _actions);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new BatchTask(_this._mode, _this._actions.length > 0 ? _this._actions : null);
      });
      _defineProperty(_assertThisInitialized(_this), "resume", function () {
        if (_this._stopped) {
          _this._running = true;
          _this._stopped = false;
          _this.notifyResumed();
          var l = _this._actions instanceof Array ? _this._actions.length : 0;
          while (--l > -1) {
            var _ref = _this._actions[l] || {},
                action = _ref.action;
            if (action) {
              if (action.hasOwnProperty('resume')) {
                action.resume();
              } else {
                _this.next(action);
              }
            }
          }
        } else {
          _this.run();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        if (!_this._running) {
          _this.notifyStarted();
          _this._currents.clear();
          _this._stopped = false;
          _this._current = null;
          if (_this._actions.length > 0) {
            var actions = [];
            _this._actions.forEach(function (entry) {
              var _ref2 = entry || {},
                  action = _ref2.action;
              if (action) {
                actions.push(action);
                _this._currents.set(action, entry);
              }
            });
            actions.forEach(function (action) {
              action.run();
            });
          } else {
            _this.notifyFinished();
          }
        }
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {
        if (_this._running) {
          var l = _this._actions instanceof Array ? _this._actions.length : 0;
          while (--l > -1) {
            var _ref3 = _this._actions[l] || {},
                action = _ref3.action;
            if (action.hasOwnProperty('stop') && action.stop instanceof Function) {
              action.stop();
            }
          }
          _this._running = false;
          _this._stopped = true;
          _this.notifyStopped();
        }
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        _current: {
          value: null,
          writable: true
        },
        _currents: {
          value: new ArrayMap(),
          writable: true
        },
        _next: {
          value: new BatchTaskNext(_assertThisInitialized(_this))
        }
      });
      return _this;
    }
    _createClass(BatchTask, [{
      key: "current",
      get: function get() {
        return this._current;
      }
    }]);
    return BatchTask;
  }(TaskGroup);

  const notEmpty = value => (typeof value === 'string' || value instanceof String) && value !== '';

  class Property {
    constructor(name = null) {
      this.name = notEmpty(name) ? name : null;
    }
    toString = () => "[" + this.constructor.name + "]";
  }

  class Attribute extends Property {
    constructor(name = null, value = undefined) {
      super(name);
      this.value = value;
    }
  }

  class Method extends Property {
    constructor(name = null, args = null) {
      super(name);
      this.args = args instanceof Array ? args : null;
    }
  }

  var Cache = function (_Action) {
    _inherits(Cache, _Action);
    var _super = _createSuper(Cache);
    function Cache() {
      var _this;
      var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var init = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      _classCallCheck(this, Cache);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "add", function (property) {
        if (property instanceof Property) {
          _this._queue.push(property);
        }
        return _assertThisInitialized(_this);
      });
      _defineProperty(_assertThisInitialized(_this), "addAttribute", function (name, value) {
        if (notEmpty(name)) {
          _this._queue.push(new Attribute(name, value));
        }
        return _assertThisInitialized(_this);
      });
      _defineProperty(_assertThisInitialized(_this), "addMethod", function (name) {
        if (notEmpty(name)) {
          for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            args[_key - 1] = arguments[_key];
          }
          _this._queue.push(new Method(name, [].concat(args)));
        }
        return _assertThisInitialized(_this);
      });
      _defineProperty(_assertThisInitialized(_this), "addMethodWithArguments", function (name, args) {
        if (notEmpty(name)) {
          _this._queue.push(new Method(name, args));
        }
        return _assertThisInitialized(_this);
      });
      _defineProperty(_assertThisInitialized(_this), "clear", function () {
        _this._queue.length = 0;
      });
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Cache(_this.target, _this._queue);
      });
      _defineProperty(_assertThisInitialized(_this), "isEmpty", function () {
        return _this._queue.length === 0;
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (_this.target) {
          var len = _this._queue.length;
          if (len > 0) {
            for (var i = 0; i < len; i++) {
              var item = _this._queue.shift();
              if (item instanceof Method) {
                _this._invokeMethod(item);
              } else if (item instanceof Attribute) {
                _this._invokeAttribute(item);
              }
            }
          }
        }
        _this.notifyFinished();
      });
      _defineProperty(_assertThisInitialized(_this), "_invokeAttribute", function (item) {
        if (item instanceof Attribute) {
          var name = item.name;
          if (notEmpty(name)) {
            _this.target[name] = item.value;
          }
        }
      });
      _defineProperty(_assertThisInitialized(_this), "_invokeMethod", function (item) {
        if (item instanceof Method && _this.target) {
          var args = item.args,
              name = item.name;
          if (notEmpty(name)) {
            var method = _this.target[name];
            if (method instanceof Function) {
              method.apply(_this.target, args);
            }
          }
        }
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        target: {
          value: target,
          writable: true
        },
        _queue: {
          value: [],
          writable: true
        }
      });
      if (init instanceof Array && init.length > 0) {
        init.forEach(function (prop) {
          return _this._queue.push(prop);
        });
      }
      return _this;
    }
    _createClass(Cache, [{
      key: "length",
      get: function get() {
        return this._queue.length;
      }
    }]);
    return Cache;
  }(Action);

  var Call = function (_Action) {
    _inherits(Call, _Action);
    var _super = _createSuper(Call);
    function Call() {
      var _this;
      var func = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var scope = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      _classCallCheck(this, Call);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Call(_this.func, _this.scope);
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (_this.func instanceof Function) {
          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }
          if (args && args.length > 0) {
            _this.func.apply(_this.scope, args);
          } else {
            _this.func.call(_this.scope);
          }
        } else {
          throw new TypeError('[Call] run failed, the \'func\' property must be a Function.');
        }
        _this.notifyFinished();
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        func: {
          value: func instanceof Function ? func : null,
          writable: true
        },
        scope: {
          value: scope,
          writable: true
        }
      });
      return _this;
    }
    return Call;
  }(Action);

  var ChainNext = function (_Receiver) {
    _inherits(ChainNext, _Receiver);
    var _super = _createSuper(ChainNext);
    function ChainNext() {
      var _this;
      var _chain = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      _classCallCheck(this, ChainNext);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "receive", function () {
        if (_this.chain === null) {
          return;
        }
        var chain = _this.chain;
        var mode = chain._mode;
        if (chain._current) {
          if (mode !== TaskGroup.EVERLASTING) {
            if (mode === TaskGroup.TRANSIENT || mode === TaskGroup.NORMAL && chain._current.auto) {
              chain._current.action.finishIt.disconnect(_assertThisInitialized(_this));
              chain._position--;
              chain._actions.splice(_this._position, 1);
            }
          }
          chain.notifyChanged();
          chain._current = null;
        }
        if (chain._actions.length > 0) {
          if (chain.hasNext()) {
            chain._current = chain._actions[chain._position++];
            chain.notifyProgress();
            if (chain._current && chain._current.action) {
              chain._current.action.run();
            } else {
              _this.receive();
            }
          } else if (_this.looping) {
            chain._position = 0;
            if (chain.numLoop === 0) {
              chain.notifyLooped();
              chain._currentLoop = 0;
              _this.receive();
            } else if (chain._currentLoop < chain.numLoop) {
              chain._currentLoop++;
              chain.notifyLooped();
              _this.receive();
            } else {
              chain._currentLoop = 0;
              chain.notifyFinished();
            }
          } else {
            chain._currentLoop = 0;
            chain._position = 0;
            chain.notifyFinished();
          }
        } else {
          chain.notifyFinished();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        return '[' + _this.constructor.name + ']';
      });
      _this.chain = _chain;
      return _this;
    }
    return ChainNext;
  }(Receiver);

  var Chain = function (_TaskGroup) {
    _inherits(Chain, _TaskGroup);
    var _super = _createSuper(Chain);
    function Chain() {
      var _this;
      var looping = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var numLoop = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var mode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'normal';
      var actions = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      _classCallCheck(this, Chain);
      _this = _super.call(this, mode, actions);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Chain(_this.looping, _this.numLoop, _this._mode, _this._actions.length > 0 ? _this._actions : null);
      });
      _defineProperty(_assertThisInitialized(_this), "element", function () {
        return _this.hasNext() ? _this._actions[_this._position].action : null;
      });
      _defineProperty(_assertThisInitialized(_this), "hasNext", function () {
        return _this._position < _this._actions.length;
      });
      _defineProperty(_assertThisInitialized(_this), "resume", function () {
        if (_this._stopped) {
          _this._running = true;
          _this._stopped = false;
          _this.notifyResumed();
          if (_this._current && _this._current.action) {
            if ("resume" in _this._current.action) {
              _this._current.action.resume();
            }
          } else {
            _this._next.receive();
          }
        } else {
          _this.run();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        if (!_this._running) {
          _this.notifyStarted();
          _this._current = null;
          _this._stopped = false;
          _this._position = 0;
          _this._currentLoop = 0;
          _this._next.receive();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {
        if (_this._running) {
          if (_this.current) {
            var _this$current$action = _this.current.action;
            _this$current$action = _this$current$action === void 0 ? {} : _this$current$action;
            var stop = _this$current$action.stop;
            if (stop instanceof Function) {
              stop();
            }
          }
          _this._running = false;
          _this._stopped = true;
          _this.notifyStopped();
        }
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        looping: {
          value: Boolean(looping),
          writable: true
        },
        numLoop: {
          value: numLoop > 0 ? Math.round(numLoop) : 0,
          writable: true
        },
        _current: {
          value: null,
          writable: true
        },
        _currentLoop: {
          value: 0,
          writable: true
        },
        _position: {
          value: 0,
          writable: true
        },
        _next: {
          value: new ChainNext(_assertThisInitialized(_this))
        }
      });
      return _this;
    }
    _createClass(Chain, [{
      key: "current",
      get: function get() {
        return this._current ? this._current.action : null;
      }
    }, {
      key: "currentLoop",
      get: function get() {
        return this._currentLoop;
      }
    }, {
      key: "position",
      get: function get() {
        return this._position;
      }
    }]);
    return Chain;
  }(TaskGroup);

  var Do = function (_Action) {
    _inherits(Do, _Action);
    var _super = _createSuper(Do);
    function Do() {
      var _this;
      _classCallCheck(this, Do);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Do();
      });
      _defineProperty(_assertThisInitialized(_this), "something", function () {
        throw new Error(_assertThisInitialized(_this) + ' something not implemented');
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (_this.something instanceof Function) {
          _this.something();
        }
        _this.notifyFinished();
      });
      return _this;
    }
    return Do;
  }(Action);

  let object;
  try {
    object = global || null;
  } catch (ignore) {}
  if (!object) {
    try {
      object = window;
    } catch (e) {}
  }
  if (!object) {
    try {
      object = document;
    } catch (ignore) {}
  }
  if (!object) {
    object = {};
  }
  const global = object;

  const performance$1 = global.performance || {};
  Object.defineProperty(global, 'performance', {
    value: performance$1,
    configurable: true,
    writable: true
  });
  performance$1.now = performance$1.now || performance$1.mozNow || performance$1.msNow || performance$1.oNow || performance$1.webkitNow;
  if (!(global.performance && global.performance.now)) {
    const startTime = Date.now();
    global.performance.now = () => Date.now() - startTime;
  }

  const ONE_FRAME_TIME = 16;
  let lastTime = Date.now();
  const vendors = ['ms', 'moz', 'webkit', 'o'];
  let len = vendors.length;
  for (let x = 0; x < len && !global.requestAnimationFrame; ++x) {
    const p = vendors[x];
    global.requestAnimationFrame = global[`${p}RequestAnimationFrame`];
    global.cancelAnimationFrame = global[`${p}CancelAnimationFrame`] || global[`${p}CancelRequestAnimationFrame`];
  }
  if (!global.requestAnimationFrame) {
    global.requestAnimationFrame = callback => {
      if (typeof callback !== 'function') {
        throw new TypeError(`${callback}is not a function`);
      }
      const currentTime = Date.now();
      let delay = ONE_FRAME_TIME + lastTime - currentTime;
      if (delay < 0) {
        delay = 0;
      }
      lastTime = currentTime;
      return setTimeout(() => {
        lastTime = Date.now();
        callback(performance$1.now());
      }, delay);
    };
  }
  if (!global.cancelAnimationFrame) {
    global.cancelAnimationFrame = id => clearTimeout(id);
  }
  global.cancelAnimationFrame;
  const requestAnimationFrame = global.requestAnimationFrame;

  var FPMS = 0.06;
  var FrameTimer = function (_Task) {
    _inherits(FrameTimer, _Task);
    var _super = _createSuper(FrameTimer);
    function FrameTimer() {
      var _this;
      _classCallCheck(this, FrameTimer);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "stopped", function () {
        return _this._stopped;
      });
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new FrameTimer();
      });
      _defineProperty(_assertThisInitialized(_this), "resume", function () {
        if (_this._stopped) {
          _this._running = true;
          _this._stopped = false;
          _this.notifyResumed();
          _this._requestID = requestAnimationFrame(_this._next.bind(_assertThisInitialized(_this)));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "reset", function () {
        _this.stop();
        _this._stopped = false;
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        if (!_this._running) {
          _this._stopped = false;
          _this.lastTime = performance.now();
          _this.notifyStarted();
          _this._requestID = requestAnimationFrame(_this._next.bind(_assertThisInitialized(_this)));
        }
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {
        if (_this._running && !_this._stopped) {
          _this._running = false;
          _this._stopped = true;
          requestAnimationFrame(_this._requestID);
          _this._requestID = null;
          _this.notifyStopped();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        return '[FrameTimer]';
      });
      _defineProperty(_assertThisInitialized(_this), "_next", function () {
        var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : performance.now();
        if (_this._requestID !== null && (_this._stopped || !_this._running)) {
          requestAnimationFrame(_this._requestID);
          _this._requestID = null;
          return;
        }
        var elapsedMS;
        if (time > _this.lastTime) {
          elapsedMS = _this.elapsedMS = time - _this.lastTime;
          if (elapsedMS > _this._maxElapsedMS) {
            elapsedMS = _this._maxElapsedMS;
          }
          _this.deltaTime = elapsedMS * FPMS * _this.speed;
          _this.notifyProgress();
        } else {
          _this.deltaTime = _this.elapsedMS = 0;
        }
        _this.lastTime = time;
        _this._requestID = requestAnimationFrame(_this._next.bind(_assertThisInitialized(_this)));
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        deltaTime: {
          value: 1,
          writable: true
        },
        elapsedMS: {
          value: 1 / FPMS,
          writable: true
        },
        lastTime: {
          value: 0,
          writable: true
        },
        speed: {
          value: 1,
          writable: true
        },
        _requestID: {
          value: null,
          writable: true
        },
        _maxElapsedMS: {
          value: 100,
          writable: true
        },
        _stopped: {
          value: false,
          writable: true
        }
      });
      return _this;
    }
    _createClass(FrameTimer, [{
      key: "fps",
      get: function get() {
        return 1000 / this.elapsedMS;
      }
    }, {
      key: "minFPS",
      get: function get() {
        return 1000 / this._maxElapsedMS;
      },
      set: function set(fps) {
        this._maxElapsedMS = 1 / Math.min(Math.max(0, fps) / 1000, FPMS);
      }
    }]);
    return FrameTimer;
  }(Task);

  var Lockable = function Lockable() {
    var _this = this;
    _classCallCheck(this, Lockable);
    _defineProperty(this, "isLocked", function () {
      return _this.__lock__;
    });
    _defineProperty(this, "lock", function () {
      _this.__lock__ = true;
    });
    _defineProperty(this, "unlock", function () {
      _this.__lock__ = false;
    });
    Object.defineProperties(this, {
      __lock__: {
        writable: true,
        value: false
      }
    });
  }
  ;

  var isLockable = function isLockable(target) {
    if (target) {
      return target instanceof Lockable || Boolean(target['isLocked']) && target.isLocked instanceof Function && Boolean(target['lock']) && target.lock instanceof Function && Boolean(target['unlock']) && target.unlock instanceof Function;
    }
    return false;
  };

  var Lock = function (_Action) {
    _inherits(Lock, _Action);
    var _super = _createSuper(Lock);
    function Lock(target) {
      var _this;
      _classCallCheck(this, Lock);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Lock(_this.target);
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (isLockable(_this.target) && !_this.target.isLocked()) {
          _this.target.lock();
        }
        _this.notifyFinished();
      });
      _this.target = target;
      return _this;
    }
    return Lock;
  }(Action);

  var Priority =
  function Priority() {
    _classCallCheck(this, Priority);
    Object.defineProperties(this, {
      priority: {
        get: function get() {
          return this._priority;
        },
        set: function set(value) {
          this._priority = value > 0 || value < 0 ? value : 0;
        }
      },
      _priority: {
        writable: true,
        value: 0
      }
    });
  };

  var Resetable = function Resetable() {
    var _this = this;
    _classCallCheck(this, Resetable);
    _defineProperty(this, "reset", function () {
      throw new Error(_this + ' reset not implemented');
    });
  };

  var Resumable = function Resumable() {
    var _this = this;
    _classCallCheck(this, Resumable);
    _defineProperty(this, "resume", function () {
      throw new Error(_this + ' resume not implemented');
    });
  };

  var Startable = function Startable() {
    var _this = this;
    _classCallCheck(this, Startable);
    _defineProperty(this, "start", function () {
      throw new Error(_this + ' start not implemented');
    });
  };

  var Stoppable = function Stoppable() {
    var _this = this;
    _classCallCheck(this, Stoppable);
    _defineProperty(this, "stop", function () {
      throw new Error(_this + ' stop not implemented');
    });
  };

  function Enum(value, name) {
    Object.defineProperties(this, {
      _name: {
        value: typeof name === "string" || name instanceof String ? name : "",
        enumerable: false,
        writable: true,
        configurable: true
      },
      _value: {
        value: isNaN(value) ? 0 : value,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  }
  Enum.prototype = Object.create(Object.prototype);
  Enum.prototype.constructor = Enum;
  Enum.prototype.equals = function (object) {
    if (object === this) {
      return true;
    }
    if (object instanceof Enum) {
      return object.toString() === this.toString() && object.valueOf() === this.valueOf();
    }
    return false;
  };
  Enum.prototype.toString = function () {
    return this._name;
  };
  Enum.prototype.valueOf = function () {
    return this._value;
  };

  var TimeoutPolicy = function (_Enum) {
    _inherits(TimeoutPolicy, _Enum);
    var _super = _createSuper(TimeoutPolicy);
    function TimeoutPolicy(value, name) {
      _classCallCheck(this, TimeoutPolicy);
      return _super.call(this, value, name);
    }
    return TimeoutPolicy;
  }(Enum);
  Object.defineProperties(TimeoutPolicy, {
    INFINITY: {
      value: new TimeoutPolicy(0, 'infinity'),
      enumerable: true
    },
    LIMIT: {
      value: new TimeoutPolicy(1, 'limit'),
      enumerable: true
    }
  });

  var Timer = function (_Task) {
    _inherits(Timer, _Task);
    var _super = _createSuper(Timer);
    function Timer() {
      var _this;
      var delay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var repeatCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var useSeconds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      _classCallCheck(this, Timer);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Timer(_this._delay, _this._repeatCount, _this._useSeconds);
      });
      _defineProperty(_assertThisInitialized(_this), "resume", function () {
        if (_this._stopped) {
          _this._running = true;
          _this._stopped = false;
          _this._itv = setInterval(_this._next.bind(_assertThisInitialized(_this)), _this._useSeconds ? _this._delay * 1000 : _this._delay);
          _this.notifyResumed();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "reset", function () {
        if (_this.running) {
          _this.stop();
        }
        _this._count = 0;
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        if (!_this._running) {
          _this._count = 0;
          _this._stopped = false;
          _this.notifyStarted();
          _this._itv = setInterval(_this._next.bind(_assertThisInitialized(_this)), _this._useSeconds ? _this._delay * 1000 : _this._delay);
        }
      });
      _defineProperty(_assertThisInitialized(_this), "stop", function () {
        if (_this._running && !_this._stopped) {
          _this._running = false;
          _this._stopped = true;
          clearInterval(_this._itv);
          _this.notifyStopped();
        }
      });
      _defineProperty(_assertThisInitialized(_this), "_next", function () {
        _this._count++;
        _this.notifyProgress();
        if (_this._repeatCount > 0 && _this._repeatCount === _this._count) {
          clearInterval(_this._itv);
          _this.notifyFinished();
        }
      });
      Object.defineProperties(_assertThisInitialized(_this), {
        _count: {
          value: 0,
          writable: true
        },
        _delay: {
          value: delay > 0 ? delay : 0,
          writable: true
        },
        _itv: {
          value: 0,
          writable: true
        },
        _repeatCount: {
          value: repeatCount > 0 ? repeatCount : 0,
          writable: true
        },
        _stopped: {
          value: false,
          writable: true
        },
        _useSeconds: {
          value: Boolean(useSeconds),
          writable: true
        }
      });
      return _this;
    }
    _createClass(Timer, [{
      key: "currentCount",
      get: function get() {
        return this._count;
      }
    }, {
      key: "delay",
      get: function get() {
        return this._delay;
      },
      set: function set(value) {
        if (this._running) {
          throw new Error(this + " the 'delay' property can't be changed during the running phase.");
        }
        this._delay = value > 0 ? value : 0;
      }
    }, {
      key: "repeatCount",
      get: function get() {
        return this._repeatCount;
      },
      set: function set(value) {
        this._repeatCount = value > 0 ? value : 0;
      }
    }, {
      key: "stopped",
      get: function get() {
        return this._stopped;
      }
    }, {
      key: "useSeconds",
      get: function get() {
        return this._useSeconds;
      },
      set: function set(flag) {
        if (this._running) {
          throw new Error(this + " the 'useSeconds' property can't be changed during the running phase.");
        }
        this._useSeconds = Boolean(flag);
      }
    }]);
    return Timer;
  }(Task);

  var Unlock = function (_Action) {
    _inherits(Unlock, _Action);
    var _super = _createSuper(Unlock);
    function Unlock(target) {
      var _this;
      _classCallCheck(this, Unlock);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "clone", function () {
        return new Unlock(_this.target);
      });
      _defineProperty(_assertThisInitialized(_this), "run", function () {
        _this.notifyStarted();
        if (Lockable(_this.target) && _this.target.isLocked()) {
          _this.target.unlock();
        }
        _this.notifyFinished();
      });
      _this.target = target;
      return _this;
    }
    return Unlock;
  }(Action);

  var isResetable = function isResetable(target) {
    if (target) {
      if (target instanceof Resetable) {
        return true;
      }
      return Boolean(target['reset']) && target.reset instanceof Function;
    }
    return false;
  };

  var isResumable = function isResumable(target) {
    return target ? target instanceof Resumable || Boolean(target['resume']) && target.resume instanceof Function : false;
  };

  var isRunnable = function isRunnable(target) {
    if (target) {
      return target instanceof Runnable || target.hasOwnProperty('run') && target.run instanceof Function;
    }
    return false;
  };

  var isStartable = function isStartable(target) {
    return target ? target instanceof Startable || Boolean(target['start']) && target.start instanceof Function : false;
  };

  var isStoppable = function isStoppable(target) {
    return target ? target instanceof Stoppable || Boolean(target['stop']) && target.stop instanceof Function : false;
  };

  /**
   * The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
   * @summary The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
   * @namespace system.process
   * @memberof system
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   */
  var data = {
    isLockable: isLockable,
    isResetable: isResetable,
    isResumable: isResumable,
    isRunnable: isRunnable,
    isStartable: isStartable,
    isStoppable: isStoppable,
    Action: Action,
    ActionEntry: ActionEntry,
    Apply: Apply,
    Batch: Batch,
    BatchTask: BatchTask,
    Cache: Cache,
    Call: Call,
    Chain: Chain,
    Do: Do,
    FrameTimer: FrameTimer,
    Lock: Lock,
    Lockable: Lockable,
    Priority: Priority,
    Resetable: Resetable,
    Resumable: Resumable,
    Runnable: Runnable,
    Startable: Startable,
    Stoppable: Stoppable,
    Task: Task,
    TaskGroup: TaskGroup,
    TaskPhase: TaskPhase,
    TimeoutPolicy: TimeoutPolicy,
    Timer: Timer,
    Unlock: Unlock
  };

  var skip = false;
  function sayHello(name = '', version = '', link = '') {
    if (skip) {
      return;
    }
    try {
      if (navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        const args = [`\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`, 'background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #000000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'background: #ff0000; padding:5px 0;'];
        window.console.log.apply(console, args);
      } else if (window.console) {
        window.console.log(`${name} ${version} - ${link}`);
      }
    } catch (error) {
    }
  }
  function skipHello() {
    skip = true;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-process'
    },
    description: {
      enumerable: true,
      value: 'A library allow you to create and manage asynchronous operations in your applications'
    },
    version: {
      enumerable: true,
      value: '1.0.6'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-process'
    }
  });
  var bundle = _objectSpread2({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.process.js.map
