"use strict" ;

import { babel }       from '@rollup/plugin-babel'
import cleanup         from 'rollup-plugin-cleanup'
import license         from 'rollup-plugin-license'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import replace         from '@rollup/plugin-replace'
import { terser }      from "rollup-plugin-terser"

import pkg from './package.json'

import setting from './build/config.json'

let mode ;
let file = setting.output + setting.file ;

try
{
    const { env : { MODE } = {} } = process ;
    if( MODE === 'prod' )
    {
        mode = 'prod' ;
        file += '.min.js' ;
    }
    else
    {
        mode = 'dev' ;
        file += '.js' ;
    }
}
catch (e) {}

const plugins =
[
    nodeResolve(),
    babel({ babelHelpers: 'bundled' }),
    replace
    ({
        delimiters        : [ '<@' , '@>' ] ,
        preventAssignment : true ,
        values            :
        {
            NAME        : pkg.name ,
            DESCRIPTION : pkg.description ,
            HOMEPAGE    : pkg.homepage ,
            LICENSE     : pkg.license ,
            VERSION     : pkg.version
        }
    }),
    cleanup()
];

if( mode === 'prod' )
{
    plugins.push( terser() ) ;
}

plugins.push( license({ banner : setting.header }) ) ;

export default
{
    input  : setting.entry ,
    output :
    {
        name      : setting.bundle ,
        file      : file  ,
        format    : 'umd' ,
        sourcemap : setting.sourcemap && (mode === 'dev') ,
        strict    : true
    },
    watch : { exclude : 'node_modules/**' },
    plugins
} ;
