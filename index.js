"use strict" ;

import Action        from './src/Action'
import ActionEntry   from './src/ActionEntry'
import Apply         from './src/Apply'
import Batch         from './src/Batch'
import BatchTask     from './src/BatchTask'
import Cache         from './src/Cache'
import Call          from './src/Call'
import Chain         from './src/Chain'
import Do            from './src/Do'
import FrameTimer    from './src/FrameTimer'
import Lock          from './src/Lock'
import Lockable      from './src/Lockable'
import Priority      from './src/Priority'
import Resetable     from './src/Resetable'
import Resumable     from './src/Resumable'
import Runnable      from './src/Runnable'
import Startable     from './src/Startable'
import Stoppable     from './src/Stoppable'
import Task          from './src/Task'
import TaskGroup     from './src/TaskGroup'
import TaskPhase     from './src/TaskPhase'
import TimeoutPolicy from './src/TimeoutPolicy'
import Timer         from './src/Timer'
import Unlock        from './src/Unlock'

import isLockable  from './src/isLockable'
import isResetable from './src/isResetable'
import isResumable from './src/isResumable'
import isRunnable  from './src/isRunnable'
import isStartable from './src/isStartable'
import isStoppable from './src/isStoppable'

/**
 * The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @summary The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @namespace system.process
 * @memberof system
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
export default
{
    isLockable   ,
    isResetable  ,
    isResumable  ,
    isRunnable   ,
    isStartable  ,
    isStoppable  ,
    
    Action        ,
    ActionEntry   ,
    Apply         ,
    Batch         ,
    BatchTask     ,
    Cache         ,
    Call          ,
    Chain         ,
    Do            ,
    FrameTimer    ,
    Lock          ,
    Lockable      ,
    Priority      ,
    Resetable     ,
    Resumable     ,
    Runnable      ,
    Startable     ,
    Stoppable     ,
    Task          ,
    TaskGroup     ,
    TaskPhase     ,
    TimeoutPolicy ,
    Timer         ,
    Unlock
} ;